import { defineConfig } from "cypress";

export default defineConfig({
  reporter: 'cypress-mochawesome-reporter',
  defaultCommandTimeout: 10000,
  watchForFileChanges: false,
  video: false,
  e2e: {
    setupNodeEvents(on, config) {
     
    },
  },
});
