describe('Actions with custom commands', () => {
	it('should visit homepage with custom command', () => {
		cy.visitHomepage();
	});

	it('should login with custom command', () => {
		cy.visitLoginpage()
		cy.fixture('loginData').then(({ username, password }) => {
			cy.login(username, password);
		})
	});
});
