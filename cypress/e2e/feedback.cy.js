describe('Feedback Test', () => {
    it('Should submit feedback form', () => {
        cy.visitHomepage();
        cy.visitFeedbackPage();
        
       cy.fixture('feedbackData').then(({ name, email, subject, message }) => {
            cy.submitFeedbackPage(name, email, subject, message);
        })
    });
});