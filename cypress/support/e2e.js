import 'cypress-mochawesome-reporter/register';

// Import commands.js using ES2015 syntax:
import './commands';
import './demoaction';
import './userActions';
import './feedback';

// Alternatively you can use CommonJS syntax:
// require('./commands')