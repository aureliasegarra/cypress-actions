Cypress.Commands.add('visitFeedbackPage', () => {
	cy.visit('http://zero.webappsecurity.com/index.html');
	cy.get('#feedback').click();
	cy.url().should('include', '/feedback.html');
});

Cypress.Commands.add('submitFeedbackPage', (name, email, subject, message) => {
	cy.get('#name').type(name)
	cy.get('#email').type(email)
	cy.get('#subject').type(subject)
	cy.get('#comment').type(message)
	cy.contains('Send Message').click()
});
