Cypress.Commands.add('login', (username, password) => {
	cy.get('#user_login').type(username);
	cy.get('#user_password').type(password);
	cy.contains('Sign in').click();
});
